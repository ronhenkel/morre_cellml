package de.unirostock.configuration;

public class Property {
	
	public class ModelType{
		/*
		 * Model Type Properties
		 */
		public final static  String SBML = "SBML";
		public final static  String CELLML = "CELLML";
		public final static  String XML = "XML";
	
	}
	
	public class General{
		/*
		 * General Properties
		 */
		public final static  String NODETYPE = "NODETYPE";
		public final static  String ID = "ID";
		public final static  String NAME = "NAME";
		public final static  String CREATED = "CREATED";
		public final static  String MODIFIED = "MODIFIED";
		public final static  String CREATOR = "CREATOR";
		public final static  String ENCODER = "ENCODER";
		public final static  String SUBMITTER = "SUBMITTER";
		public final static  String AUTHOR = "AUTHOR";
		public final static  String EMAIL = "EMAIL";
		public final static  String URI = "URI";
		public final static  String FILENAME = "FILENAME";
		public final static  String DATABASEID = "DATABASEID";
		public final static  String RESOURCETEXT = "RESOURCETEXT";
		public final static  String NONRDF = "NONRDF";
		public final static  String IS_INDEXED = "ISINDEXED";
	
	}
	
	public class XML{
	/*
	 * Properties to store XML in DB
	 */
		public final static  String VALUE = "VALUE";
		public final static  String ELEMENT = "ELEMENT";
		public final static  String ATTRIBUTE_NAMES = "ATTRIBUTENAMES";
		public final static  String ATTRIBUTE_VALUES = "ATTRIBUTEVALUES";
	}
	
	public class SBML{
	/*
	 * Properties to store SBML in DB
	 */	
		public final static  String VERSION = "VERSION";
		public final static  String LEVEL = "LEVEL";
		//public final static  String NAME = "NAME";
		public final static  String COMPARTMENT = "COMPARTMENT";
		public final static  String REACTION = "REACTION";
		public final static  String SPECIES = "SPECIES";
		public final static  String PARAMETER = "PARAMETER";
		public final static  String RULE = "RULE";
		public final static  String FUNCTION = "FUNCTION";
		public final static  String EVENT = "EVENT";
	}
	
	public class CellML{
	/*
	 * Properties to store CellML in DB
	 */	
		public final static  String VERSION = "VERSION";
		//public final static  String NAME = "NAME";
		public final static  String COMPONENT = "COMPONENT";
		//public final static  String GROUP = "GROUP";
		public final static  String VARIABLE = "VARIABLE";
		//public final static  String CONNECTION = "CONNECTION";	
		public final static  String REVERSIBLE = "REVERSIBLE";
		public final static  String REACTIONDIRECTION = "REACTIONDIRECTION";
	}
	
	public class NodeType{
		/*
		 * Properties to determine the type of a node in the DB
		 */	
		public final static  String MODEL = "MODEL";
		public final static  String DOCUMENT = "DOCUMENT";		
		public final static  String ANNOTATION = "ANNOTATION";
		public final static  String PERSON = "PERSON";
		public final static  String RESOURCE = "RESOURCE";
		public final static  String PUBLICATION = "PUBLICATION";
		
		//SBML specific
		public final static  String SBML_COMPARTMENT = "SBMLCOMPARTMENT";
		public final static  String SBML_REACTION = "SBMLREACTION";
		public final static  String SBML_SPECIES = "SBMLSPECIES";
		public final static  String SBML_PARAMETER = "SBMLPARAMETER";
		public final static  String SBML_RULE = "SBMLRULE";
		public final static  String SBML_FUNCTION = "SBMLFUNCTION";
		public final static  String SBML_EVENT = "SBMLEVENT";
		
		//CellML specific
		public final static  String CELLML_COMPONENT = "CELLMLCOMPONENT";
		public final static  String CELLML_REACTION = "CELLMLREACTION";	
		//public final static  String CELLML_GROUP = "CELLMLGROUP";
		public final static  String CELLML_VARIABLE = "CELLMLVARIABLE";
		//public final static  String CELLML_CONNECTION = "CELLMLCONNECTION";		

		
	}	

	public class Publication{
	/*
	 * Properties to store Publication in DB
	 */
		public final static  String AUTHOR = "AUTHOR";
		public final static  String TITLE = "TITLE";
		public final static  String ABSTRACT = "ABSTRACT";
		public final static  String JOURNAL = "JOURNAL";
		public final static  String YEAR = "YEAR";
		public final static  String AFFILIATION = "AFFILIATION";
		public final static  String SYNOPSIS = "SYNOPSIS";
		public final static  String ID = "PUBID";
		
	}	
	
	public class Person{
	/*
	 * Properties to store Publication in DB
	 */
		public final static  String FAMILYNAME = "FAMILYNAME";
		public final static  String GIVENNAME = "GIVENNAME";
		public final static  String EMAIL = "EMAIL";
		public final static  String ORGANIZATION = "ORGANIZATION";		
	}	
	
}
