package de.unirostock.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.neo4j.graphdb.Node;

import de.unirostock.configuration.Property;
import de.unirostock.database.traverse.DBTraverser;

public class Database {
	
	public static List<String> getAllStoredDocumentURIs(){
		List<String> uriList = new LinkedList<>();
		List<Node> documentNodes = DBTraverser.getDocumentsFromDatabase();
		for (Iterator<Node> iterator = documentNodes.iterator(); iterator.hasNext();) {
			Node docNode = (Node) iterator.next();
			uriList.add((String)docNode.getProperty(Property.General.URI, ""));
		}
		return uriList;
	}
	
	public static List<String> getAllStoredDocumentFilenames(){
		List<String> uriList = new LinkedList<>();
		List<Node> documentNodes = DBTraverser.getDocumentsFromDatabase();
		for (Iterator<Node> iterator = documentNodes.iterator(); iterator.hasNext();) {
			Node docNode = (Node) iterator.next();
			uriList.add((String)docNode.getProperty(Property.General.FILENAME, ""));
		}
		return uriList;
	}

}
