package de.unirostock.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.unirostock.query.results.ModelResultSet;


public class ResultSetUtil {
	/**
	 * This comparator orders the models by score descending
	 * @author Ron Henkel
	 *
	 */
	private static class ModelResultSetScoreComperator implements Comparator<ModelResultSet>{

		@Override
		public int compare(ModelResultSet rs1, ModelResultSet rs2) {
			if ((rs1==null) || (rs2==null)) return 0; 
			
			if (rs1.getScore() < rs2.getScore())  return 1;
			if (rs1.getScore() > rs2.getScore()) return -1;
			
			return 0;
		}
		
	}
	
	/**
	 * This comparator orders the models by database id ascending
	 * @author Ron Henkel
	 *
	 */
	private static class ModelResultSetDatabaseIDComperator implements Comparator<ModelResultSet>{

		@Override
		public int compare(ModelResultSet rs1, ModelResultSet rs2) {
			if ((rs1==null) || (rs2==null)) return 0;
			
			if (rs1.getDatabaseId() > rs2.getDatabaseId())  return 1;
			if (rs1.getDatabaseId() < rs2.getDatabaseId()) return -1;
			
			return 0;
		}
		
	}
	
	public static List<ModelResultSet> sortModelResultSetByScore(List<ModelResultSet> rsList){

		Collections.sort(rsList, new ModelResultSetScoreComperator());
		return rsList;
	}
	
	public static List<ModelResultSet> sortModelResultSetByDatabaseId(List<ModelResultSet> rsList){

		Collections.sort(rsList, new ModelResultSetDatabaseIDComperator());
		return rsList;
	}
	
	public static List<ModelResultSet> collateModelResultSetByDatabaseId(List<ModelResultSet> rsList){
		HashMap<Long, ModelResultSet> toBeKept = new HashMap<Long, ModelResultSet>();
		for (Iterator<ModelResultSet> rsListIt = rsList.iterator(); rsListIt.hasNext();) {
			ModelResultSet resultSet = (ModelResultSet) rsListIt.next();
			if (toBeKept.keySet().contains(resultSet.getDatabaseId())){
				ModelResultSet toBeKeptResultSet = toBeKept.get(resultSet.getDatabaseId());
				if (toBeKeptResultSet.getScore() < resultSet.getScore()) toBeKept.put(resultSet.getDatabaseId(), resultSet); 
			} else {
				toBeKept.put(resultSet.getDatabaseId(), resultSet);
			}
		}
		return sortModelResultSetByDatabaseId(new LinkedList<ModelResultSet>(toBeKept.values()));
	}

	
	public static List<ModelResultSet> collateModelResultSetByModelId(List<ModelResultSet> rsList){
		Map<String, ModelResultSet> toBeKept = new HashMap<String, ModelResultSet>();
		Map<String, Integer> freq = new HashMap<String, Integer>();		
		int count; 
		for (Iterator<ModelResultSet> rsListIt = rsList.iterator(); rsListIt.hasNext();) {
			ModelResultSet resultSet = (ModelResultSet) rsListIt.next();
			if (toBeKept.keySet().contains(resultSet.getModelId())){
				//count occurrences of a model
				count = freq.containsKey(resultSet.getModelId()) ? freq.get(resultSet.getModelId()) : 0;
				freq.put(resultSet.getModelId(), count + 1);
				//store model to list
				ModelResultSet toBeKeptResultSet = toBeKept.get(resultSet.getModelId());
				float score = toBeKeptResultSet.getScore() + resultSet.getScore();
				toBeKeptResultSet.setScore(score);
				toBeKept.put(resultSet.getModelId(), toBeKeptResultSet); 
			} else {
				toBeKept.put(resultSet.getModelId(), resultSet);
			}
		}
		
		int maxCount = 1;
		for (Iterator<Integer> countIt = freq.values().iterator(); countIt.hasNext();) {
			int c = countIt.next();
			if (c > maxCount) maxCount = c;
		}
		for (Iterator<ModelResultSet> iterator = toBeKept.values().iterator(); iterator.hasNext();) {
			ModelResultSet modelResultSet = (ModelResultSet) iterator.next();
			modelResultSet.setScore(modelResultSet.getScore()/maxCount);			
		}
		return sortModelResultSetByScore(new LinkedList<ModelResultSet>(toBeKept.values()));
	}

}
