package de.unirostock.query;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.unirostock.query.results.AnnotationResultSet;
import de.unirostock.query.results.ModelResultSet;
import de.unirostock.query.results.PersonResultSet;
import de.unirostock.query.results.PublicationResultSet;
import de.unirostock.query.types.AnnotationQuery;
import de.unirostock.query.types.PersonQuery;
import de.unirostock.query.types.PublicationQuery;

public class QueryAdapter {

	
	public static List<ModelResultSet> executeSingleQueryForModels(IQueryInterface iq){
		return iq.getModelResults();
		
	}
	
	public static List<AnnotationResultSet> executeAnnotationQuery(AnnotationQuery aq){
		return aq.getResults();
		
	}
	
	public static List<PersonResultSet> executePersonQuery(PersonQuery persq){
		return persq.getResults();
		
	}
	
	public static List<PublicationResultSet> executePublicationQuery(PublicationQuery pubq){
		return pubq.getResults();
		
	}
	
	public static List<ModelResultSet> executeMultipleQueriesForModels(List<IQueryInterface> iqList){
		List<ModelResultSet> rs = new LinkedList<ModelResultSet>();
		for (Iterator<IQueryInterface> iqIt = iqList.iterator(); iqIt.hasNext();) {
			IQueryInterface interfaceQuery = (IQueryInterface) iqIt.next();	
			rs.addAll(interfaceQuery.getModelResults());
		}
		return rs;
	}

/*
	private static Analyzer selectAnalyzerByEnum(IndexEnumerator e) {
		switch (e) {
		case MODELINDEX:
			return NodeFullTextIndexAnalyzer.getNodeFullTextIndexAnalyzer();
		case ANNOTATIONINDEX:
			return AnnotationIndexAnalyzer.getAnnotationIndexAnalyzer();
		case PUBLICATIONINDEX:
			return PublicationFullTextIndexAnalyzer.getPublicationFullTextIndexAnalyzer();
		case PERSONINDEX:
			return PersonExactIndexAnalyzer.getPersonExactIndexAnalyzer();	
		default:
			return null;
		}
	}

	private static Index<Node> selectIndexByEnum(IndexEnumerator e){
		switch (e) {
		case MODELINDEX:
			return nodeIndex;
		case ANNOTATIONINDEX:
			return annotationFull;
		case PUBLICATIONINDEX:
			return publicationFull;
		case PERSONINDEX:
			return personExact;	
		default:
			return null;
		}

	}

*/	
}
