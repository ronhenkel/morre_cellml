package de.unirostock.query.results;


import org.apache.commons.lang3.StringUtils;

import de.unirostock.query.IResultSetInterface;

public class ModelResultSet implements IResultSetInterface {
	private String modelName;
	private float score;
	private String modelID;
	private String explanation;
	private long databaseID;
	private String documentURI;
	private String filename;
	
	public ModelResultSet(float score, String modelId, String modelName, Long databaseID, String documentURI, String filename, String explanation){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
		this.explanation = explanation;
		this.databaseID = databaseID;
		this.documentURI = documentURI;
		this.filename = filename;
	}
	
	public ModelResultSet(float score, String modelId, String modelName, Long databaseID, String explanation){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
		this.explanation = explanation;
		this.databaseID = databaseID;
		
	}
	
	public ModelResultSet(float score, String modelId, String modelName){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
	}
	
	public ModelResultSet(float score, String modelId, String modelName, String documentURI, String filename){
		this.modelName = modelName;
		this.score = score;
		this.modelID = modelId;
		this.documentURI = documentURI;
		this.filename = filename;
	}	
	
	public String getModelName() {
		return modelName;
	}
	
	public String getModelId() {
		return modelID;
	}

	@Override
	public float getScore() {
		return score;
	}

	@Override
	public String getSearchExplanation() {
		return explanation;
	}

	public Long getDatabaseId() {
		return databaseID;
	}
	
	public String getDocumentURI() {
		return documentURI;
	}

	public void setDocumentURI(String documentURI) {
		this.documentURI = documentURI;
	}
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public void setScore(float score) {
		this.score = score;
	}

	@Override
	public boolean equals(Object resultSet) {
		ModelResultSet rs; 
		if ((resultSet==null) || !(resultSet instanceof ModelResultSet)) return false;
		else rs = (ModelResultSet) resultSet; 
		
		if (!StringUtils.equals(this.modelID, rs.getModelId())) return false;
		
		if (!StringUtils.equals(this.documentURI, rs.getDocumentURI())) return false;
		
		if (!StringUtils.equals(this.filename, rs.getFilename())) return false;
		
		if (!StringUtils.equals(this.modelName, rs.getModelName())) return false;
		
		if (!StringUtils.equals(this.explanation, rs.getSearchExplanation())) return false;
		
		if (this.score != rs.score) return false;
		
		if (this.databaseID != rs.databaseID) return false;
		
		return true;
	}


}
