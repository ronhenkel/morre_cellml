package de.unirostock.query;

public interface IResultSetInterface {
	
	public float getScore();
	
	public String getSearchExplanation();
}
