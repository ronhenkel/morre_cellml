package de.unirostock.query.enumerator;


public enum IndexEnumerator {

	MODELINDEX,
	ANNOTATIONINDEX,
	PUBLICATIONINDEX,
	PERSONINDEX,
	CONSTITUENTINDEX;
	
	public String getElementNameEquivalent() {
		switch (this) {
		case MODELINDEX:
			return "MODELINDEX";
		case ANNOTATIONINDEX:
			return "ANNOTATIONINDEX";
		case PUBLICATIONINDEX:
			return "PUBLICATIONINDEX";
		case PERSONINDEX:
			return "PERSONINDEX";	
		case CONSTITUENTINDEX:
			return "CONSTITUENTINDEX";		
		default:
			return "unknown";
		}
	} 
}
