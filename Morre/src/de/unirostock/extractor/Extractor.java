package de.unirostock.extractor;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;

import de.unirostock.configuration.Property;
import de.unirostock.configuration.Relation;
import de.unirostock.configuration.Relation.DatabaseRelTypes;
import de.unirostock.configuration.Relation.DocumentRelTypes;
import de.unirostock.data.PersonWrapper;
import de.unirostock.data.PublicationWrapper;
import de.unirostock.database.Manager;
import de.unirostock.extractor.CellML.CellMLExtractor;
import de.unirostock.extractor.SBML.SBMLExtractor;
import de.unirostock.extractor.XML.XMLExtractor;
import de.unirostock.query.enumerator.PersonFieldEnumerator;
import de.unirostock.query.enumerator.PublicationFieldEnumerator;
import de.unirostock.query.types.PersonQuery;
import de.unirostock.query.types.PublicationQuery;

public abstract class Extractor {
	protected static Index<Node> publicationIndex = Manager.instance().getPublicationIndex();
	protected static Index<Node> personIndex = Manager.instance().getPersonIndex();
	protected static Index<Node> modelIndex = Manager.instance().getModelIndex();	
	protected static Index<Relationship> relationshipIndex = Manager.instance().getRelationshipIndex();
	protected static Index<Node> annotationIndex = Manager.instance().getAnnotationIndex();
	protected static Index<Node> constitientIndex = Manager.instance().getConstituentIndex();
	protected static GraphDatabaseService graphDB = Manager.instance().getDatabase();
	
	public static Node setExternalDocumentInformation(Node documentNode, Map<String, String> propertyMap){
		if (propertyMap==null) return documentNode;
		
		for (Iterator<String> iterator = propertyMap.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			Transaction tx = graphDB.beginTx();
			try {
				documentNode.setProperty(key, propertyMap.get(key));
				tx.success();
			} catch (IllegalArgumentException e) {
				tx.failure();
			} finally {
				tx.finish();
			}
			
		}
		return documentNode;
	}
	
	public static Node setExternalDocumentInformation(Node documentNode, String filepath, String documentURI ){
		if (StringUtils.isEmpty(documentURI)) documentURI = "";
		if (StringUtils.isEmpty(filepath)) filepath = "";
			Transaction tx = graphDB.beginTx();
			try {
				documentNode.setProperty(Property.General.FILENAME, filepath);
				documentNode.setProperty(Property.General.URI, documentURI);
				tx.success();
			} catch (IllegalArgumentException e) {
				tx.failure();
			} finally {
				tx.finish();
			}
			

		return documentNode;
	}
	
	public static Node extractStoreIndex(String path, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new FileInputStream(path), null, null);									 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(path, null, null);
		default : return XMLExtractor.extractStoreIndex(new FileInputStream(path), null, null);
		}		

	}

	public static Node extractStoreIndex(byte[] byteArray, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new ByteArrayInputStream(byteArray), null, null);									 
		case 	Property.ModelType.CELLML : {
			
			File temp = File.createTempFile("graphstore_model_temp", "xml");
			FileUtils.writeByteArrayToFile(temp, byteArray);			
			Node n = CellMLExtractor.extractStoreIndex(temp.getPath(), null, null);
			temp.delete();
			return n;
		}
		default : return XMLExtractor.extractStoreIndex(new ByteArrayInputStream(byteArray), null, null);
		}	
		
	}
	
	public static Node extractStoreIndex(File file, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new FileInputStream(file), null, null);									 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(file.getPath(), null, null);
		default : return XMLExtractor.extractStoreIndex(new FileInputStream(file), null, null);
		}			
	}
	
	public static Node extractStoreIndex(String path, PublicationWrapper publication, Long databaseID, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new FileInputStream(path), publication, databaseID);									 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(path, publication, databaseID);
		default : return XMLExtractor.extractStoreIndex(new FileInputStream(path), publication, databaseID);
		}		
	}

	public static Node extractStoreIndex(byte[] byteArray, PublicationWrapper publication, Long databaseID, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new ByteArrayInputStream(byteArray), publication, databaseID);										 
		case 	Property.ModelType.CELLML : {
			
			File temp = File.createTempFile("graphstore_model_temp", "xml");
			FileUtils.writeByteArrayToFile(temp, byteArray);			
			Node n = CellMLExtractor.extractStoreIndex(temp.getPath(), publication, databaseID);
			temp.delete();
			return n;
		}
		default : return XMLExtractor.extractStoreIndex(new ByteArrayInputStream(byteArray), publication, databaseID);
		}
	}
	
	public static Node extractStoreIndex(File file, PublicationWrapper publication, Long databaseID, String modelType)
			throws XMLStreamException, IOException {		
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new FileInputStream(file), publication, databaseID);										 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(file.getPath(), publication, databaseID);
		default : return XMLExtractor.extractStoreIndex(new FileInputStream(file), publication, databaseID);
		}
		
	}
	
	protected static void processPublication(PublicationWrapper publication, Node referenceNode, Node modelNode) {
		//all Strings null -> ""
		publication.repairNullStrings();
		
		Node publicationNode = null;
		try {
			PublicationQuery pq = new PublicationQuery();
			pq.addQueryClause(PublicationFieldEnumerator.PUBID, publication.getPubid());			
			publicationNode = publicationIndex.query(pq.getQuery()).getSingle();
		} catch (NoSuchElementException  e) {
			publicationNode = null;
		}
		if (publicationNode==null) {
			publicationNode = graphDB.createNode();	
			
			publicationNode.setProperty(Property.Publication.ID, publication.getPubid());
			publicationIndex.add(publicationNode,Property.Publication.ID, publication.getPubid());
			publicationNode.setProperty(Property.Publication.ABSTRACT, publication.getSynopsis());
			publicationIndex.add(publicationNode,Property.Publication.ABSTRACT, publication.getSynopsis());
			publicationNode.setProperty(Property.Publication.ABSTRACT, publication.getSynopsis());
			publicationIndex.add(publicationNode,Property.Publication.ABSTRACT, publication.getSynopsis());
			publicationNode.setProperty(Property.Publication.AFFILIATION, publication.getAffiliation());
			publicationIndex.add(publicationNode,Property.Publication.AFFILIATION, publication.getAffiliation());
			publicationNode.setProperty(Property.Publication.JOURNAL, publication.getJounral());
			publicationIndex.add(publicationNode,Property.Publication.JOURNAL, publication.getJounral());
			publicationNode.setProperty(Property.Publication.TITLE, publication.getTitle());
			publicationIndex.add(publicationNode,Property.Publication.TITLE, publication.getTitle());
			publicationNode.setProperty(Property.Publication.YEAR, publication.getYear());
			publicationIndex.add(publicationNode,Property.Publication.YEAR, publication.getYear());
			
			for (Iterator<PersonWrapper> iterator = publication.getAuthors().iterator(); iterator.hasNext();) {
				PersonWrapper author = (PersonWrapper) iterator.next();
				processPerson(author, modelNode, publicationNode, DocumentRelTypes.HAS_AUTHOR);			
			}
			publicationNode.setProperty(Property.General.NODETYPE, Property.NodeType.PUBLICATION);
			publicationNode.createRelationshipTo(referenceNode, DatabaseRelTypes.BELONGS_TO);
			referenceNode.createRelationshipTo(publicationNode, DocumentRelTypes.HAS_PUBLICATION);
		}
	}
	
	protected static void processPerson(PersonWrapper person, Node modelNode, Node referenceNode, RelationshipType relationToReference){
		Node personNode = null;
		person.repairNullStrings();
		try {
			PersonQuery pq = new PersonQuery();
			pq.addQueryClause(PersonFieldEnumerator.FAMILYNAME, person.getLastName());
			pq.addQueryClause(PersonFieldEnumerator.GIVENNAME, person.getFirstName());
			personNode = personIndex.query(pq.getQuery()).getSingle();
		} catch (NoSuchElementException  e) {
			personNode = null;
		}					
		//create a node for each creator & link persons between models
		if (personNode==null){
			personNode= graphDB.createNode();
			personNode.setProperty(Property.General.NODETYPE, Property.NodeType.PERSON);
			personNode.setProperty(Property.Person.GIVENNAME, person.getFirstName());
			personNode.setProperty(Property.Person.FAMILYNAME, person.getLastName());
			personNode.setProperty(Property.Person.ORGANIZATION, person.getOrganization());
			personNode.setProperty(Property.Person.EMAIL, person.getEmail());
			//add to person index
			personIndex.add(personNode, Property.Person.FAMILYNAME, person.getLastName());
			personIndex.add(personNode, Property.Person.GIVENNAME, person.getFirstName());
			personIndex.add(personNode, Property.Person.EMAIL, person.getEmail());
			personIndex.add(personNode, Property.Person.ORGANIZATION, person.getOrganization());

			//add to node index
			if (relationToReference.equals(DocumentRelTypes.IS_CREATOR)) {
				modelIndex.add(modelNode, Property.General.CREATOR, person.getFirstName());
				modelIndex.add(modelNode, Property.General.CREATOR, person.getLastName());
			} else {
				modelIndex.add(modelNode, Property.General.AUTHOR, person.getFirstName());
				modelIndex.add(modelNode, Property.General.AUTHOR, person.getLastName());
			}
		}

		//set relationships
		personNode.createRelationshipTo(referenceNode, Relation.DatabaseRelTypes.BELONGS_TO);
		referenceNode.createRelationshipTo(personNode, relationToReference);
	}
	


}
