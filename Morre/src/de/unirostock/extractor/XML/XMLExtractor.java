package de.unirostock.extractor.XML;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLStreamException;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import de.unirostock.data.PublicationWrapper;
import de.unirostock.extractor.Extractor;

public class XMLExtractor extends Extractor{

	
	public static Node extractStoreIndex(InputStream stream, PublicationWrapper publication, Long databaseID) throws XMLStreamException, IOException{
		
		Node documentNode = null;
		Transaction tx = graphDB.beginTx();
		try {
			documentNode = extractFromXML(stream, publication, databaseID);
		} catch (XMLStreamException e) {
			documentNode = null;
			//TODO Log me
			System.out.println("Error XMLStreamException while parsing model");
			System.out.println(e.getMessage());
		} finally {
			if (documentNode!=null) {
				tx.success();
			} else { 
				tx.failure();
			}
			tx.finish();
		}
		//if (!wasSuccessful) throw new XMLStreamException();
		return documentNode;
	}

	private static Node extractFromXML(InputStream stream,
			PublicationWrapper publication, Long databaseID) throws XMLStreamException {
		throw new XMLStreamException("Format not supported, yet");
		
	}
}
