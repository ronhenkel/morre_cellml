package de.unirostock.extractor.CellML;

import java.util.concurrent.Callable;

import org.neo4j.graphdb.Node;

import de.unirostock.configuration.Property;
import de.unirostock.data.PublicationWrapper;
import de.unirostock.extractor.Extractor;

public class CellMLExtractorThread implements Callable<Node> {

	private String filePath;
	private PublicationWrapper publication;
	private Long databaseID;
	
	public CellMLExtractorThread(String filePath, PublicationWrapper publication, Long databaseID){
		this.databaseID = databaseID;
		this.publication = publication;
		this.filePath = filePath;
	}
	
	public CellMLExtractorThread(String filePath){
		this.databaseID = null;
		this.publication = null;
		this.filePath = filePath;
	}
	
	@Override
	public Node call() throws Exception {
		
		return Extractor.extractStoreIndex(filePath,publication,databaseID,Property.ModelType.CELLML);
	}

}
