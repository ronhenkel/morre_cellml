package de.unirostock.annotation;

import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.StringUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.graphdb.index.ReadableIndex;

import de.unirostock.configuration.Property;
import de.unirostock.database.Manager;

public class AnnotationResolverUtil {
	
	private static AnnotationResolverUtil INSTANCE = null;
	
	private volatile Boolean indexLocked = false;
	
	private AtomicLong urlCount= null;
	private ExecutorService urlThreadPool = Executors.newFixedThreadPool(10);
	private ExecutorService uriThreadPool = Executors.newFixedThreadPool(5);
	
	public static synchronized AnnotationResolverUtil instance() {
		if (INSTANCE == null) {
			INSTANCE = new AnnotationResolverUtil();
		}
		return INSTANCE;
	}
	
	private AnnotationResolverUtil(){
		Manager.instance().getDatabase();
		urlCount= new AtomicLong(0);
	}
 
	private class WaitThreat extends Thread{
		
		@Override
		public void run() {
			try {
				sleep(5000);
			} catch (InterruptedException e) {
				
			}
		}
	}

	
	public void fillAnnotationFullTextIndex(){
		
		if (AnnotationResolverUtil.instance().isIndexLocked()){
			while (AnnotationResolverUtil.instance().isIndexLocked()){
				ExecutorService executor = Executors.newSingleThreadExecutor();
				WaitThreat wt = new WaitThreat();
				try {
					//Runtime.getRuntime().addShutdownHook(wt);
					executor.submit(wt).get();
				} catch (InterruptedException | ExecutionException e) {

				}
			}
		} else {
			AnnotationResolverUtil.instance().setIndexLocked(true);
		}
		
		System.out.println("Creating URI list from DB...");
	
		long uriCounter = 0;	
		String uri = "";
		ReadableIndex<Node> autoNodeIndex = Manager.instance().getAutoNodeIndex();
		IndexHits<Node> resourceNodeList = autoNodeIndex.get(Property.General.NODETYPE, Property.NodeType.RESOURCE);
		for (Iterator<Node> nodeIt = resourceNodeList.iterator(); nodeIt.hasNext();) {
			Node node = (Node) nodeIt.next();
			if (StringUtils.equals((String)node.getProperty(Property.General.NODETYPE, null),Property.NodeType.RESOURCE)){
				if (!(Boolean)node.getProperty(Property.General.IS_INDEXED, false)) {	
				uri = (String)node.getProperty(Property.General.URI);
				ResolveThread rt = new ResolveThread(uri, ++uriCounter);
				rt.setName(uri);
				//Runtime.getRuntime().addShutdownHook(rt);
				uriThreadPool.execute(rt);						
				}
			}
		}
		
		System.out.println("Started to resolve " + uriCounter + "URIs...");		

		uriThreadPool.shutdown();
		// Wait until all threads are finish
		while (!uriThreadPool.isTerminated()) {

		}
		uriThreadPool = Executors.newFixedThreadPool(5);
		//TODO implement logging!
		//TODO really, implement logging for this!!!
		
		System.out.println("done!");		
		
		System.out.println("Started to retrieve " + urlCount + "URLs...");
				

		urlThreadPool.shutdown();
		// Wait until all threads are finish
		while (!urlThreadPool.isTerminated()) {
	
		}
		urlThreadPool = Executors.newFixedThreadPool(10);
		System.out.println("done!");
		
		System.out.println("Annotation Index created.");
		AnnotationResolverUtil.instance().setIndexLocked(false);
	}
	
	
	public synchronized void  addToUrlThreadPool(String uri, String url){		
		FetchThread ft = new FetchThread(uri, url, urlCount.getAndIncrement());
		ft.setName(url);
		//Runtime.getRuntime().addShutdownHook(ft);
		urlThreadPool.execute(ft);
	}

	public Boolean isIndexLocked() {
		return indexLocked;
	}

	public void setIndexLocked(Boolean indexLocked) {
		this.indexLocked = indexLocked;
	}


}
