package de.unirostock.database.traverse;

import org.apache.commons.lang3.StringUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;

import de.unirostock.configuration.Property;

public class NodeTypReturnEvaluater implements Evaluator{
	private String nodeType;
	
	public NodeTypReturnEvaluater(String nodeType){
		this.nodeType = nodeType;
	}
	
//	@Override
//	public boolean isReturnableNode(TraversalPosition pos) {
//		Node node = pos.currentNode();
//		if (node.hasProperty(Property.General.NODE_TYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODE_TYPE), nodeType)){	
//		  return true;
//		} else return false;
//	}

	@Override
	public Evaluation evaluate(Path path) {
		Node node = path.endNode();
		if (node.hasProperty(Property.General.NODETYPE) && StringUtils.equals((String) node.getProperty(Property.General.NODETYPE), nodeType)){
			return Evaluation.INCLUDE_AND_PRUNE;
		} else return Evaluation.EXCLUDE_AND_CONTINUE;	
	}
}
