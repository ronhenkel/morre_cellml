package de.unirostock.database.traverse;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;

import de.unirostock.configuration.Property;
import de.unirostock.configuration.Relation.AnnotationRelTypes;
import de.unirostock.configuration.Relation.DatabaseRelTypes;
import de.unirostock.configuration.Relation.DocumentRelTypes;
import de.unirostock.database.Manager;
import de.unirostock.query.results.ModelResultSet;

public class DBTraverser {

	
	public static Node getDocumentFromModel(Node modelNode) {
		if (modelNode==null) return null;
		Node docNode = null;
		if ((modelNode!=null) && modelNode.hasProperty(Property.General.NODETYPE) && StringUtils.equals(Property.NodeType.MODEL,(String) modelNode.getProperty(Property.General.NODETYPE))){
			docNode = modelNode.getSingleRelationship(DatabaseRelTypes.BELONGS_TO, Direction.OUTGOING).getEndNode();
		} 
		if ((docNode!=null) && docNode.hasProperty(Property.General.NODETYPE) && StringUtils.equals(Property.NodeType.DOCUMENT, (String) docNode.getProperty(Property.General.NODETYPE))){
			return docNode;
		}
		return null;
		
	}
	
	

	public static Node fromNodeToAnnotation(Node node){
		if (node==null) return null;
		Relationship rs = node.getSingleRelationship(AnnotationRelTypes.HAS_ANNOTATION, Direction.OUTGOING);
		
		if (rs==null) return null;
		Node annoNode = rs.getEndNode();
		
		if ((annoNode!=null) && annoNode.hasProperty(Property.General.NODETYPE) && StringUtils.equals(Property.NodeType.ANNOTATION, (String) annoNode.getProperty(Property.General.NODETYPE))){
			return annoNode;
		}
		return null;
	}
	
	public static List<Node> getModelsFromNode(Node node){
		LinkedList<Node> modelList = new LinkedList<Node>();
		if (node==null) return modelList;
		TraversalDescription td = Traversal.description()
										.depthFirst()
	            .relationships( DatabaseRelTypes.BELONGS_TO, Direction.OUTGOING )
	            .evaluator( new NodeTypReturnEvaluater(Property.NodeType.MODEL));
		
		Traverser t = td.traverse(node);
		for (Iterator<Path> it = t.iterator(); it.hasNext();) {
			Node tNode = ((Path) it.next()).endNode();
			modelList.add(tNode);
		}		
		return modelList;
	}
	
	public static List<Node> getDocumentsFromNode(Node node){
		List<Node> modelList = getModelsFromNode(node);
		List<Node> documentList = new LinkedList<Node>();
		
		for (Iterator<Node> iterator = modelList.iterator(); iterator.hasNext();) {
			Node modelNode = (Node) iterator.next();
			documentList.add(getDocumentFromModel(modelNode));
		}		
		return documentList;
	}

	public static List<Node> getPersonFromPublication(Node publicationNode) {
		LinkedList<Node> personList = new LinkedList<Node>();
		Iterable<Relationship> ir = publicationNode.getRelationships(DocumentRelTypes.HAS_AUTHOR, Direction.OUTGOING);
		for (Iterator<Relationship> iterator = ir.iterator(); iterator.hasNext();) {
			Relationship r = (Relationship) iterator.next();
			personList.add(r.getOtherNode(publicationNode));			
		}
		return personList;
	}

	
	public static List<ModelResultSet> getModelResultSetFromNode(Node node, float score) {
		List<ModelResultSet> result = new LinkedList<ModelResultSet>();
		List<Node> modelList = getModelsFromNode(node);
		for (Iterator<Node> iterator = modelList.iterator(); iterator.hasNext();) {
			Node modelNode = (Node) iterator.next();
			Node docNode = DBTraverser.getDocumentFromModel(modelNode);
			Long databaseId = null;
			if (docNode.hasProperty(Property.General.DATABASEID)) databaseId = (Long)docNode.getProperty(Property.General.DATABASEID);
			//TODO make this generic somehow, for now it is bound to SBML
			ModelResultSet rs = new ModelResultSet(score, (String)modelNode.getProperty(Property.General.ID), (String)modelNode.getProperty(Property.General.NAME), databaseId, null);
			if (docNode.hasProperty(Property.General.URI)) rs.setDocumentURI((String)docNode.getProperty(Property.General.URI));
			if (docNode.hasProperty(Property.General.FILENAME)) rs.setFilename((String)docNode.getProperty(Property.General.FILENAME));
			result.add(rs);
			}
		return result;
	}
	
	public static List<Node> getDocumentsFromDatabase(){
		List<Node> documentNodes = new LinkedList<Node>();
		
		Node root = Manager.instance().getDatabase().getReferenceNode();
		Iterable<Relationship> ir = root.getRelationships(DocumentRelTypes.HAS_DOCUMENT, Direction.OUTGOING);
		for (Iterator<Relationship> it = ir.iterator(); it.hasNext();) {
			Relationship r = (Relationship) it.next();
			documentNodes.add(r.getOtherNode(root));			
		}
		return documentNodes;
	}




}
